import fetch from "node-fetch";
import cheerio from "cheerio";
import request from "request-promise";
import fs from "fs";

const states_string =
  "AL AK AZ AR CA CO CT DE DC FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY";
const space = " ";
const url =
  "https://www.durasupreme.com/wp-content/themes/durasupreme/ajax/find-showroom.php";

async function init() {
  const array_states = states_string.split(space);
  const $ = cheerio.load(await getRawData(url));
  //   const result = $(".result").find('a');
  //   console.log(result.html().trim());

  //   const secondA = $(".result").find('a').next();
  //   console.log(secondA.html().trim());

  //   const thirdData = $(".result").find('a').next().next();
  //   console.log(thirdData.html().trim());

  //   const fourData = $(".result").find('a').next().next().next();
  //   console.log(fourData.html().trim());
  var array_data = [];
  const container = $(".results .result").each((i, el) => {
    array_data.push($(el).text().trim());
  });

  createCSV(array_data);
}

function getRawData(url) {
  const array_states = states_string.split(space);
  const params = new URLSearchParams();
  params.append("search_type", "state");
  params.append("search_val", "CA");
  return fetch(url, { method: "POST", body: params })
    .then((response) => response.text())
    .then((data) => {
      return data;
    });
}

function createCSV(dealers) {
  var csv = "Store;Address;Phone;\n";

  dealers.forEach((row) => {
    const prueba = row.split("\n");
    csv += prueba[0] + ";" + prueba[3] + ";" + prueba[5] + "\n";
  });
  console.log(csv);
  const writeStream = fs.createWriteStream("dura-supreme.csv", "utf8");
  writeStream.write(csv);
}

init();
