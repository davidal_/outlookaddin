import fetch from "node-fetch";
import cheerio from "cheerio";
import fs from "fs";

const getRawData = (URL) => {
  return fetch(URL)
    .then((response) => response.json())
    .then((data) => {
      const dealers = data.map((d) => {
        return {
          id: d.id,
          store: d.store,
          email: d.email,
          phone: d.phone,
          address:
            d.address +
            ", " +
            d.address2 +
            ", " +
            d.city +
            ", " +
            d.state +
            ", " +
            d.zip +
            ", " +
            d.country,
        };
      });
      createCSV(dealers);
    });
};

getRawData(
  "https://www.symphony-group.co.uk/wp-admin/admin-ajax.php?action=store_search&lat=53.6335819&lng=-0.2467173&max_results=1000&search_radius=10000&filter="
);

function createCSV(dealers) {
  //console.log(dealers);
  var csv = "Store;Email;Phone;Address;\n";

    dealers.forEach((element)=>{
        element.store = element.store.replace('#038;', '');
    });

  dealers.forEach((row) => {
    csv +=
      row.store +
      ";" +
      row.email +
      ";" +
      row.phone +
      ";" +
      row.address +
      "\n";
  });
console.log(csv);
 const writeStream = fs.createWriteStream('dealers.csv', 'utf8');
 writeStream.write(csv);
}
